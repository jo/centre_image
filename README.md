# 23forward/centre_image

Un outil de direction artistique pour SPIP par ARNO* (23forward.com)
Documentation détaillée par ARNO* https://23forward.com/Plugin-centre_image

Le plugin permet de définir automatiquement et manuellement le centre d'intérêt d'une image.

Une fois le plugin installé, sur chaque image de l’espace privé (logo ou document joint), une petite croix apparaît sur l’image (parfois avec un petit délai).
Si le centre d'intérêt de image (petite croix) n’est pas positionnée à l’endroit qui convient, il suffit de la faire glisser et de la déposer au bon endroit.


## Compatibilité et prérequis du plugin SPIP

Le plugin est compatible avec [SPIP 4.0+](https://www.spip.net) et nécessite le plugin [`medias`](https://plugins.spip.net/medias.html).


## Configuration

Une fois le plugin installé et activé, par défaut, le plugin tente de détecter automatiquement le centre d'intérêt de chaque image basé sur un calcul de changement de densité dans l'image.

On peut forcer le plugin à utiliser un autre algorithme de détection automatique en ajoutant dans votre fichier `config/mes_options.php` la constante `_SPIP_CENTRE_IMAGE`.

Les valeurs suivantes sont disponibles :

 * `visage` : algorithme qui tentera de positionner le point d'intérêt sur les visages
 * `centre` : définit la position automatique du point d'intérêt au centre de l'image (pour un positionnement uniquement manuellement depuis l'espace privé)

```
defined('_SPIP_CENTRE_IMAGE') || define('_SPIP_CENTRE_IMAGE', 'visage');
```

## Utilisation

### Avec le filtre `|image_recadre` ou `|image_proportions`

La fonction `image_recadre` native de SPIP permet désormais d’utiliser le recadrage proportionnel basé sur le point d’intérêt.
Les deux lignes suivantes sont ainsi équivalentes :

```
[(#FICHIER|image_proportions{2,1,focus})]
[(#FICHIER|image_recadre{2:1,-,focus})]
```

### Centrer strictement ou en conservant le dynamisme de l’image

Le recadrage avec le critère `focus` recadre l'image en essayant de conserver une partie du dynamisme de l’image d’origine.

On a la possibilité de préférer un recadrage exactement centré sur le point d’intérêt de l'image (autant que possible, si le point d’intérêt est trop proche du bord, on ne va pas recadrer en dehors de l’image), on peut le faire en remplaçant le critère `focus` par `focus-center`.

```
[(#FICHIER|image_proportions{2,1,focus-center})]
```

### Récupérer les valeurs numériques du centre d’intérêt

Si l’on a des besoins spécifiques, on peut obtenir les valeurs x et y du point d’intérêt avec les fonctions centre_image_x et centre_image_y.
Les valeurs sont en pourcentage entre 0 (à gauche ou en haut) et 1 (à droite ou en bas).

```
[(#FICHIER|centre_image_x)] × [(#FICHIER|centre_image_y)]
```


## Exemples

Recadrer un logo d'article en carré d'une hauteur/largeur de `100px` basé sur le centre d’intérêt :

```
[(#LOGO_ARTICLE_NORMAL|image_recadre{1:1,-,focus-center}|image_reduire{100})]
```

Recadrer un logo de rubrique au ratio `16/9` avec une hauteur/largeur max de `500px`

```
[(#LOGO_RUBRIQUE|image_recadre{16:9,-,focus-center}|image_reduire{500})]
```
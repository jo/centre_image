<?php

if (!defined("_ECRIRE_INC_VERSION")) return;


function action_centre_image_forcer() {
	$fichier = $_GET["url"];

	// Gérer le plugin mutualisation si on n'est pas dans le prive
	if (defined('_DIR_SITE') and (false === strpos('../' . $fichier, _DIR_SITE))){
		$fichier = _DIR_SITE . $fichier;
	}

	if (_DIR_RACINE
		and strpos($fichier, _DIR_RACINE) !== 0
		and !file_exists($fichier)
		and file_exists(_DIR_RACINE . $fichier)) {
		$fichier = _DIR_RACINE . $fichier;
	}

	// image uniquement présente dans _DIR_IMG
	if (strpos($fichier, _DIR_IMG) === 0) {
		include_spip('inc/centre_image');
		$fichier = centre_image_preparer_fichier($fichier);

		if (file_exists($fichier)) {
			$res = array("x" => $_GET["x"], "y" => $_GET["y"]);
			centre_image_ecrire_force($fichier, $res);

			include_spip('inc/invalideur');
			suivre_invalideur('centre_image');
		}
	}
}
